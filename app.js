const express = require('express');
const mysql = require('mysql2');

const app = express();
const PORT = 3000;

// Connexion à la base de données MySQL
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'bibliotheque'
});

db.connect(error => {
    if (error) throw error;
    console.log('Connecté à la base de données MySQL.');
});

// Middleware pour parser le JSON et les données du formulaire
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Configuration du moteur de templates
app.set('view engine', 'ejs');

// Middleware pour servir des fichiers statiques
app.use(express.static('public'));

// Routes API
app.get('/api/books', (req, res) => {
    const sql = 'SELECT * FROM books';
    db.query(sql, (error, results) => {
        if (error) throw error;
        res.json(results);
    });
});

// ajouter un livre
app.post('/api/books', (req, res) => {
    const { title, author, isbn, publishedDate, copies } = req.body;
    const sql = 'INSERT INTO books (title, author, isbn, publishedDate, copies) VALUES (?, ?, ?, ?, ?)';
    db.query(sql, [title, author, isbn, publishedDate, copies], (error, results) => {
        if (error) throw error;
        res.json({ id: results.insertId, ...req.body });
    });
});

// Routes pour l'interface utilisateur
app.get('/', (req, res) => {
    res.render('home');
});

app.get('/add-book', (req, res) => {
    res.render('add-book');
});

app.get('/list-books', (req, res) => {
    const sql = 'SELECT * FROM books';
    db.query(sql, (error, results) => {
        if (error) throw error;
        res.render('list-books', { books: results });
    });
});

app.listen(PORT, () => {
    console.log(`Serveur démarré sur le port ${PORT}`);
});
